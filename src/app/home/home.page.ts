import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  weight: number;
  gender: string;
  genders =[];
  time: number;
  bottles: number;
  promille: number;

  constructor() {}

  ngOnInit(){

    this.genders.push('Male');
    this.genders.push('Female');

    this.gender= 'Female';
    this.weight= 65;
    this.time = 3;
    this.bottles = 5;
  }

  calculate(){
    
    const litres = this.bottles*0.33;
    
    let grams = litres *8 *4.5;

    const burning = this.weight/10;
    grams = grams - (burning * this.time);

    if(this.gender==='Male'){
      this.promille =grams/(this.weight*0.7);
    }else{
      this.promille =grams/(this.weight*0.6);
    }

    if(this.promille<0){
      this.promille=0;
    }

  }

}
